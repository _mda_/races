package by.dmolotchko.project.horses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Application {
	private static final int NUMBER_OF_WINNERS = 3;

	static public class RaceResult {
		final private Horse horse;
		final private int time;
		
		public RaceResult(Horse horse, int raceTime) {
			this.horse = horse;
			this.time = raceTime;
		}
		public Horse getHorse() {
			return horse;
		}
		public int getTime() {
			return time;
		}
		public int compareTo(RaceResult s) {
			return time < s.getTime() ? -1 : time > s.getTime() ? 1 : 0;
		}
	}

	private static final int NUMBER_OF_HORSES = 25;
	public static final int HORSES_PER_ROUND = 5;
	private static final int REST_TIME = 7000;

	static public class Horse implements Runnable {
		private static final int MAX_TIME_TO_RACE = 5000;
		final private String name;
		private int   raceTime;
		private CountDownLatch finishLane;
		
		public Horse(String name) {
			super();
			this.name = name;
			this.raceTime = 0;
		}
		public String getName() {
			return name;
		}
		
		public int getRaceTime() {
			return raceTime;
		}
		public void setRaceTime(int raceTime) {
			this.raceTime = raceTime;
		}
		@Override
		public void run() {
			this.raceTime = new Random().nextInt(MAX_TIME_TO_RACE);
			try {
				// run
				Thread.sleep(this.raceTime);
				// check on finish line
				finishLane.countDown();
			} catch (InterruptedException e) {
				// Do nothing
			}
		}
		public void setFinishLane(CountDownLatch finishLane) {
			this.finishLane = finishLane;
		}
		public int compareTo(Horse s) {
			return raceTime < s.getRaceTime() ? -1 : raceTime > s.getRaceTime() ? 1 : 0;
		}
	}

	public static void main(String[] args) throws InterruptedException {
		final List<Horse> horses = new ArrayList<>(NUMBER_OF_HORSES); 
		populateHorses(horses);
		
		final List<Horse> finalRace = new ArrayList<>(NUMBER_OF_HORSES);
		
		// open hippodrome
		ThreadPoolExecutor hippodrome = (ThreadPoolExecutor) Executors.newFixedThreadPool(HORSES_PER_ROUND); 
		
		// we have one hippodrome, so all races will be one by one
		for( int i=0;  i<NUMBER_OF_HORSES;  i+=HORSES_PER_ROUND ){
			System.out.printf("Race #%d started...", i/HORSES_PER_ROUND+1);
			List<RaceResult> raceWinners = getWinners(i, horses, hippodrome, 1);
			if( raceWinners.size() > 0 ){
				final RaceResult result = raceWinners.get(0);
				finalRace.add(result.getHorse());
				// publish winner
				System.out.printf(" winner: %s (%d ms)\n", result.getHorse().getName(), result.getTime());
			}
		}
		
		// give horses a rest
		System.out.println("Give horses a rest. Wait a little bit before final race.");
		Thread.sleep(new Random().nextInt(REST_TIME));
		
		// make the last race
		System.out.print("Final race started...\n");
		List<RaceResult> raceWinner = getWinners(0, finalRace, hippodrome, NUMBER_OF_WINNERS);
		raceWinner.stream().forEach(rr->{
			System.out.printf(" winner: %s (%d ms)\n", rr.getHorse().getName(), rr.getTime());
		});
		
		// close hippodrom
		hippodrome.shutdown();
	}

	public static List<RaceResult> getWinners(int i, List<Horse> horses, ThreadPoolExecutor hippodrom, int numOfWinners) throws InterruptedException {
		final CountDownLatch finishLane = new CountDownLatch(HORSES_PER_ROUND);
		final List<Horse> participants = new ArrayList<>(); 
		IntStream
			.range(i, i+HORSES_PER_ROUND)
			.forEach(ind->{
				final Horse horse = horses.get(ind);
				horse.setFinishLane(finishLane);
				participants.add(horse);
				hippodrom.execute(horse);
			});

		// wait for all horses at finish lane
		finishLane.await();
		
		Collections.sort(participants, new Comparator<Horse>() {
			@Override
			public int compare(Horse f, Horse s) {
				return f.compareTo(s);			
			}
		});
		
		// get the winner
		final List<RaceResult> raceWinners = 
		IntStream
			.range(0, numOfWinners)
			.mapToObj(
				ind->new RaceResult(participants.get(ind), participants.get(ind).getRaceTime()))
			.collect(Collectors.toList());
		return raceWinners;
		
	}

	private static void populateHorses(List<Horse> horses) {
		for(int i=0; i<NUMBER_OF_HORSES;  ++i){
			horses.add(new Horse("horse"+i));
		}
	}

}
