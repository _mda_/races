package by.dmolotchko.project.horses.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.IntStream;

import org.junit.Test;

import by.dmolotchko.project.horses.Application;
import by.dmolotchko.project.horses.Application.Horse;
import by.dmolotchko.project.horses.Application.RaceResult;

public class GetWinnerTest {

	@Test
	public void test() throws InterruptedException {
		final List<Horse> horses = new ArrayList<>(Application.HORSES_PER_ROUND); 
		populateHorses(horses);
		
		ThreadPoolExecutor hippodrome = (ThreadPoolExecutor) Executors.newFixedThreadPool(Application.HORSES_PER_ROUND); 
		
		List<RaceResult> raceWinner = Application.getWinners(0, horses, hippodrome, 1);
		if( raceWinner.size() > 0 ){
			final int winnerResult = raceWinner.get(0).getTime();
			OptionalInt minimumTime = horses.stream().mapToInt(h->h.getRaceTime()).min();
			if( minimumTime.isPresent()  &&  winnerResult == minimumTime.getAsInt() ){
				return;
			}
		}
		fail("Winner of the race is a tricker!");
	}

	private void populateHorses(List<Horse> horses) {
		IntStream
		.range(0, Application.HORSES_PER_ROUND)
		.forEach(ind->{
			horses.add(new Horse("horse"+ind));
		});
	}

}
